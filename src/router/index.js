import Vue from 'vue';
import Router from 'vue-router';
import store from '../vuex/index'
import axios from '../axios/http'
Vue.use(Router);

let router = new Router({
    routes: [{
            path: '/',
            redirect: '/home'
        },
        {
            path: '/',
            component: resolve => require(['../components/common/Home.vue'], resolve),
            meta: {
                title: ''
            },
            children: [{
                path: '/home',
                component: resolve => require(['../components/page/Bhome.vue'], resolve),
                    meta: {
                        title: '系统首页',
                        requiresAuth: true
                    }
                },
                {
                    path: '/baobao',
                    component: resolve => require(['../components/page/BaoManage.vue'], resolve),
                    meta: {
                        title: '宝宝管理',
                        requiresAuth: true
                    }
                },
                {
                    path: '/jiazhang',
                    component: resolve => require(['../components/page/JiaManage.vue'], resolve),
                    meta: {
                        title: '家长管理',
                        requiresAuth: true
                    }
                },
                {
                    path: '/employee',
                    component: resolve => require(['../components/page/Employee.vue'], resolve),
                    meta: {
                        title: '员工管理',
                        requiresAuth: true
                    }
                },
                {
                    path: '/passlog',
                    component: resolve => require(['../components/page/Passlog.vue'], resolve),
                    meta: {
                        title: '接送日志',
                        requiresAuth: true
                    }
                },
                {
                    path: '/equipment',
                    component: resolve => require(['../components/page/Equipment.vue'], resolve),
                    meta: {
                        title: '设备管理',
                        requiresAuth: true
                    }
                },
                {
                    path: '/notice',
                    component: resolve => require(['../components/page/Notice.vue'], resolve),
                    meta: {
                        title: '公告管理',
                        requiresAuth: true
                    }
                },
                {
                    path: '/role',
                    component: resolve => require(['../components/page/Role.vue'], resolve),
                    meta: {
                        title: '职位管理',
                        requiresAuth: true
                    }
                },
                {
                    path: '/room',
                    component: resolve => require(['../components/page/Room.vue'], resolve),
                    meta: {
                        title: '班级管理',
                        requiresAuth: true
                    }
                },
                {
                    path: '/dolog',
                    component: resolve => require(['../components/page/Dolog.vue'], resolve),
                    meta: {
                        title: '操作日志',
                        requiresAuth: true
                    }
                }
            ]
        },
        {
            path: '/login2019$ztkid',
            component: resolve => require(['../components/page/Login.vue'], resolve)
        },
        {
            path: '*',
            redirect: '/404'
        },
        {
            path: '/404',
            component: resolve => require(['../components/page/404.vue'], resolve)
        }
    ]
})

router.beforeEach((to, from, next) => {
    let data = window.sessionStorage.getItem('ztkids')
    if (data) {
      //登录信息
      store.commit({
        type: 'userInfo',
        data: JSON.parse(data)
      })
    }
    if (to.matched.some(record => record.meta.requiresAuth)) {
        let adminId = store.getters.adminId
        axios.post('/login/GetMySession.do', {})
        .then((response) => {
            let result_data = response.data;
            if (parseInt(result_data.status) === 1) {
                if (adminId) {
                    next()
                } else {
                    next({
                        path: '/login2019$ztkid'
                    })
                }
            } else {
                next({
                    path: '/login2019$ztkid'
                })
            }
        })
        .catch((error) => {
            console.log(error.message)
            next()
        })
    } else {
      next() // 确保一定要调用 next()
    }
})

export default router;

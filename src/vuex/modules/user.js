import axios from '../../axios/http'
import router from '../../router/index'
const user = {
  state: {
    name: '',
    adminId: 0,
    companyId: 0,
    menus: [],
    phone: '',
    companyName: ''
  },
  mutations: {
    userInfo(state, payload) {
      let data = payload.data;
      state.adminId = data.id === undefined ? 0: data.id;
      state.companyId = data.companyId === undefined ? 0 : data.companyId;
      state.name = data.name === undefined ? '' : data.name;
      state.phone = data.phone === undefined ? '' : data.phone;
      state.companyName = data.companyName === undefined ? '' : data.companyName;
    },
    menuList(state, payload) {
      state.menus = payload.menus
    }
  },
  actions: {

  }
}

export default user